#include <iostream>
#include <cstdint>

// elapsed_time 828

int32_t a_plus_b(int32_t a, int32_t b)
{
    if (a > 30000 || b > 30000)
        return -1;
    return a + b;
}
