#include <iostream>

#include "tasks/a_plus_b.h"

int32_t main()
{
#if 0 // a_plus_b
    int32_t a = 37;
    int32_t b = 12;
    std::cout << std::to_string(a) << " + " << std::to_string(b) << " = " << std::to_string(a_plus_b(37, 12)) << "\n";
#endif // !a_plus_b
    return 0;
}
